// get all the DOM objects used
var ViewPlayer = {
    container:document.getElementById('player'),
    logoBox:document.getElementById('logobox'),
    logo:document.getElementById('logo'),
    playPauseButton:document.getElementById('playerbutton'),
    playPauseButtonIcon:document.getElementById('playicon'),
    streamStatus:document.getElementById('streamstatus')
}